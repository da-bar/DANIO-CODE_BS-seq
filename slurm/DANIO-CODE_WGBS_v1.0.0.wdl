workflow DANIO-CODE_WGBS {

	File reads1
	File reads2
	File index
	File chrSizes
	String bam_root
	String rrbs

	call bismarkAlign {
		input: reads1=reads1, reads2=reads2, index=index, bam_root=bam_root, rrbs=rrbs
	}
	call extractMethylation {
		input: bam=bismarkAlign.bamOut, mapReport=bismarkAlign.report, index=index, bam_root=bam_root 
	}
	call generateSignal {
		input: bg=extractMethylation.bedGraph, chrSizes=chrSizes, bam_root=bam_root
	}
	call generateRegion {
		input: cxReport=extractMethylation.cxReport, chrSizes=chrSizes, bam_root=bam_root

	}

}

task bismarkAlign{

	command {
		../wgbs/dme-align-pe/dname_align_pe.sh ${index} ${reads1} ${reads2} 100 5000 8 ${bam_root} ${rrbs}
	}

	output {
		File bamOut = "${bam_root}.bam",
		File report="${bam_root}_map_report.txt",
		File qc="${bam_root}_qc.txt"

	}
}

task extractMethylation {

	command {
		../wgbs/dme-extract-pe/dname_extract_pe.sh ${index} ${bam} 8
	}

	output {
		File cxReport="${bam_root}.CX_report.txt.gz",
		File bedGraph="${bam_root}.bedGraph.gz",
		File mBias="${bam_root}_mbias_report.txt"

	}
}

task generateRegion {

	command {
		../wgbs/dme-cx-to-bed/dname_cx_to_bed.sh ${cxReport} ${chrSizes}
	}

	output {
		File cpgBed="${target_root}_CpG.bed"
		File cpgBigBed="${target_root}_CpG.bb"
		File chgBed="${target_root}_CHG.bed"
		File chgBigBed="${target_root}_CHG.bb"
		File chhBed="${target_root}_CHH.bed"
		File chhBigBed="${target_root}_CHH.bb"
	}
}

task generateSignal {

	command {
		../wgbs/dme-bg-to-signal/dname_bg_to_signal.sh ${target_root}.bedGraph $chrom_size
	}

	output {
		File bigWig="${bam_root}.bigWig"
	}
}