#uses the output files from either sambamba_deduplicate.sh, filter_noncon_PE.sh or filter_noncon_SE.sh
#change MethylDackel_DIR and genome_DIR. Also change line 7 "*deduplicated_filter.bam" to *deduplicated.bam IF filter_noncon step was skipped
#uses MethylDackel options which also filters out SNPs 

MethylDackel_DIR="/PATH/"
genome_DIR="/PATH/"
for input_bam in *deduplicated_filter.bam; do
	${MethylDackel_DIR}MethylDackel extract  --mergeContext  --minOppositeDepth 1 --maxVariantFrac 0.1 ${genome_DIR}genome.fa $input_bam
	

done
