#uses the output files from sambamba_deduplicate.sh
#change samtools_DIR and picard_DIR
#this script is an optional step and will remove reads with 4 or more non-converted C's in non CG contexts. This is good for removing samples which have unconverted read contamination. Not recommended for adult tissue samples

samtools_DIR="/PATH/"
picard_DIR="/PATH/"

for input_bam in *SE_sorted_deduplicated.bam; do
ext='_filter.bam'
ext2='_filter_list'
output_bam=$(echo $input_bam| rev | cut -c 5- | rev)$ext
filter_list=$(echo $input_bam | rev | cut -c 5- | rev)$ext2

${samtools_DIR}samtools view -f2 $input_bam  | awk '{if ($2==16) {print $1,gsub(/TG|AG|GG/,"",$10)} else if ($2==0) {print $1,gsub(/CC|CA|CT/,"",$10)}}' | awk '{if ($2>3) {print $1}}' > $filter_list

#view -f2 only takes into account reads that are mapped correctly in pairs, so it is quicker as these are the only reads that methyldackel uses
For single end use 2==16 for OB and 2==0 for OT. the 2>3 refers to cut off for non CG nonconversion count.

#remove the marked non-converted reads from the original BAM files using picard
java -jar ${picard_DIR}picard.jar  FilterSamReads I=$input_bam O=$output_bam READ_List_File=$filter_list Filter=excludeReadList

done
