#change ADAPTERS_DIR path and Trimmomatic_DIR Path. Also check fastq files end with SE.fastq.gz, change line 6 of code to match if not the case

ADAPTERS_DIR="/PATH/"
Trimmomatic_DIR="/PATH/"

for read_1 in *SE.fastq.gz; do #check the name of fastq file read_1
   echo $read_1
       ext_paired='trimmed_SE.fastq.gz'
       output_paired_1=$(echo $read_1 | rev | cut -c 12- | rev)$ext_paired
   trimmomatic_command="java -jar ${Trimmomatic_DIR}trimmomatic-0.38.jar SE -threads 24 $read_1 $output_paired_1 \
   ILLUMINACLIP:{ADAPTERS_DIR}adapters-SE.fa:2:30:10 SLIDINGWINDOW:5:20 LEADING:5 TRAILING:5 MINLEN:40" 
   echo $trimmomatic_command && eval $trimmomatic_command
 done
