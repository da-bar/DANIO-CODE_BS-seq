**Description of the pipeline:**


Quality filtering and trimming of raw reads (Trimmomatic)
*Trim_PE.sh or Trim_SE.sh*

Mapping Bisulfite converted reads to the reference genome (Walt and sambambda)
*Walt_PE.sh or Walt_SE.sh*

Deduplication of reads (sambamba)
*sambamba_deduplicate.sh*

Removal of fully unconverted reads in non CG contexts (optional)(samtools and picard)
*filter_noncon_PE.sh or filter_noncon_SE.sh*

Methylation calling and SNP removal (MethylDackel)
*MethylDackel_callmC_filterSNP*

**Outputs**


**Mapped reads**

Format: bam
File: *sorted.bam


**Filtered and deduplicated reads**

Format: bam
File: *sorted_deduplicated.bam


**Filtered for non converted reads**

Format: bam
File: *sorted_deduplicated_filtered.bam


**Methylation calls**
Format: bedGraph
File: *CpG.bedGraph