#change ADAPTERS_DIR path and Trimmomatic_DIR Path. Also check fastq files end with 1.fastq.gz and 2.fastq.gz, change line 6 of code to match if not the case

ADAPTERS_DIR="/PATH/"
Trimmomatic_DIR="/PATH/"

for read_1 in *1.fastq.gz; do #check the name of fastq file read_1
   echo $read_1
       read_2=$(echo $read_1 | sed s/1.fastq/2.fastq/) # check the read_2 name
       echo $read_2
       ext_paired='trimmed_paired.fastq.gz'
       ext_unpaired='trimmed_unpaired.fastq.gz'
       output_paired_1=$(echo $read_1 | rev | cut -c 10- | rev)$ext_paired
       output_unpaired_1=$(echo $read_1 | rev | cut -c 10- | rev)$ext_unpaired
       output_paired_2=$(echo $read_2 | rev | cut -c 10- | rev)$ext_paired
       output_unpaired_2=$(echo $read_2 | rev | cut -c 10- | rev)$ext_unpaired
       
   trimmomatic_command="java -jar ${Trimmomatic_DIR}trimmomatic-0.38.jar PE -threads 24 $read_1 $read_2 $output_paired_1 $output_unpaired_1 $output_paired_2 $output_unpaired_2 \
   ILLUMINACLIP:${ADAPTERS_DIR}adapters-PE.fa:2:30:10:8:TRUE SLIDINGWINDOW:5:20 LEADING:5 TRAILING:5 MINLEN:40" 
   echo $trimmomatic_command && eval $trimmomatic_command