#uses the output files from Trim_SE.sh
#change WALT_DIR, DBindex_DIR  AND SAMBAMBA_DIR.
WALT_DIR="/PATH/"
DBindex_DIR="/PATH/"
SAMBAMBA_DIR="/PATH/"
   
for read_1 in *trimmed_SE.fastq.gz; do
	echo $read_1
        read_3=$(echo $read_1 | rev | cut -c 4- | rev)
		
        ext='.sam'
		ext2='_sorted.bam'
        output_sam=$(echo $read_1 | rev | cut -c 10- | rev)$ext 
		output_bam=$(echo $read_1 | rev | cut -c 10- | rev)$ext2
	gunzip -c $read_1 > $read_3 #gunzip fastq files but keep a zipped copy, as the unzipped will be deleted soon


	${WALT_DIR}walt -i ${DBindex_DIR}genome.dbindex  -r $read_3 -o $output_sam  -t 24 -N 10000000 -L 2000
	
	rm $read_3
	
	#convert sam to bam and sort
	
	${SAMBAMBA_DIR}sambamba view -t 16 -S -f bam -h $output_sam| ${SAMBAMBA_DIR}sambamba sort -t 16 -o $output_bam /dev/stdin
	
	rm $output_sam #optional step to save space

done
