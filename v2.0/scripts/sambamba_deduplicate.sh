#uses the output files from either Walt_PE.sh or Walt_SE.sh 
#change SAMBAMBA_DIR


SAMBAMBA_DIR="/PATH/"
for read_1 in *sorted.bam; do
	echo $read_1
        ext='_deduplicated.bam'
        output_bam=$(echo $read_1 | rev | cut -c 5- | rev)$ext 


	${SAMBAMBA_DIR}sambamba markdup -r -t 16 $read_1 $output_bam
	
	

done